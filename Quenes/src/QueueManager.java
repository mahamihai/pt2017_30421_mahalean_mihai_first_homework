import java.io.File;
import java.util.ArrayList;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTextArea;

public class QueueManager {
	ArrayList<Queue> queues;
	ArrayList<JTextArea> displays;
	private static final Logger LOGGER = Logger.getLogger( QueueManager.class.getName() );
	
    // PUBLISH this level
   
	public  QueueManager(int nr_queues)
	{
		this.queues=new ArrayList<Queue>();
		for(int i=0;i<nr_queues;i++)
		{
			Queue aux=new Queue();
			this.queues.add(aux);
		}
		
	}
	
	public void  set_views(ArrayList<JTextArea> displays)
	{
		this.displays=displays;
	}
	public void print_all()
	{
		for(Queue aux:this.queues)
		{
			System.out.println(aux.toString());
		}
	}
	public void print_all_2_gui()
	{
		for(int i=0;i<this.queues.size();i++)
		{
			displays.get(i).setText(this.queues.get(i).toString());
			
		}
	}
	public void pass_second(int current_time )
	{
		ArrayList<Thread>threads=new ArrayList<Thread>(this.queues.size());
		for(Queue aux:this.queues)
		{
			Thread thread=new Thread(new Runnable()
					{
				public void run()
				{
					aux.second_passed();
					aux.recalculate(current_time);
				}
			}
			);
			thread.start();
		}
		for(Thread aux:threads)
		{
			try {
				aux.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		this.print_all_2_gui();
		
	}
	public void add_person(int time_needed,int current_time)	//add a person to the fastest queue
	{
	
		int min_wait=32000;
		
		Queue tmp=null;
		int i=1;
		int selected=0;
		for(Queue aux:this.queues)
		{
			int wait_time=aux.waiting_time();
			if(wait_time<min_wait)
			{
				selected=i;
				min_wait=wait_time;
				tmp=aux;
			}
			i++;
			
			
		}
		QueueManager.LOGGER.log(Level.INFO, "A new person with the service time of "+time_needed+"has been added to the queue "+selected);
		tmp.add_person(time_needed);
		tmp.recalculate(current_time);
		
		
	}
	
	
	

}
