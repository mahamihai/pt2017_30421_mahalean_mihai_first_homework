import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Queue {
	private static final Logger LOGGER = Logger.getLogger( Queue.class.getName() );
	
	ArrayList <Person> persons;
	ArrayList<Person> waited;
	private int average_wait;
	private int total_wait;
	private int peak_time=0;
	private int peak_amount=0;
	public Queue()
	{	this.total_wait=0;
		this.average_wait=0;
		this.waited=new ArrayList<Person>();
		this.persons=new ArrayList<Person>();
	}
	public void add_person(int time_needed)
	{
		Person aux=new Person(time_needed);
		persons.add(aux);
	}
	public void second_passed( )
	{
		
		
		if(persons.size()>0)
		{
			
			for(Person aux:persons)
			{
				aux.second_passed();
				
			}
			if (persons.get(0).first_ready())
			{
				Queue.LOGGER.log(Level.INFO, "A person with the waiting time of "+persons.get(0).get_waited()+" has been been served and removed from the queue ");
				waited.add(persons.get(0));
				persons.remove(0);
			}
		}
	}
	private void calculate_wait()
	{
		int aux=0;
		
		for(Person a:this.persons)
		{
			aux+=a.get_waited();
		}
		this.total_wait=aux;	
	}
	
	private int calculate_waited()
	{
		int aux=0;
		
		for(Person a:this.waited)
		{
			aux+=a.get_waited();
		}
		return aux;
		
	}
	
	private void calculate_average()
	{
		if(this.waited.size()>0)
		{
			this.average_wait=calculate_waited()/this.waited.size();
		}
		else
		{
			this.average_wait=0;
		}
	}
	public void recalculate(int current_time)
	{
		calculate_wait();
		calculate_average();
		if(waiting_time()>peak_amount)
		{
			peak_amount=waiting_time();
			this.peak_time=current_time;
		}
		
		
	}
	public int waiting_time()
	{
		int aux=0;
		
		for(Person a:this.persons)
		{
			aux+=a.get_need();
		}
		
		return aux;
	}
	
	public int get_wait()
	{
		return total_wait;	
	}
	@Override
	public String toString()
	{
		int i=1;
		String tmp="Queue=total_wait:"+this.waiting_time()+" average time:"+this.average_wait+" peak time:"+this.peak_time+"\n"+"ID:Serving-Waited\n";
		for(Person aux:this.persons)
		{
			tmp+="C"+i +":"+aux.get_need()+"-"+aux.get_waited();
			if(i==1)
				tmp+="<-Currently served";
			tmp+="\n";
			i++;
		}
		
		return tmp;
	}

	

}
