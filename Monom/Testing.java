
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class Testing {
	@Test
    public void test_printing() {
            Polynom tester = new Polynom(); // MyClass is tested
            tester.add_coeff(10, 5);
            tester.add_coeff(-15, 14);
            // assert statements
            assertEquals("10 x 0 must be 0", "10*x^5+-15*x^14",tester.print_polynom());
    }
	@Test
    public void addition_test() {	//check addition of two monoms
								
           Monom m1 = new IntegerMonom(5,6); // MyClass is tested
           Monom m2=new IntegerMonom(2,6);
           m1.add(m2);
          
            // assert statements
            assertEquals("5x^6+2x^6 must be 7x^6","7x^6",m1.toStr()+"x^"+m1.get_grad());
    }
	@Test
    public void subtraction_test() {	//test subtraction of 2 Monoms
           Monom m1 = new IntegerMonom(5,6); // MyClass is tested
           Monom m2=new RealMonom(2,6);
           m1=m1.real_subtract(m2);
          
            // assert statements
            assertEquals("5x^6-2x^6 must be 3x^6","3x^6",m1.toStr()+"x^"+m1.get_grad());
    }
	@Test
    public void multiply_test() {		//test multiplication of 2 Monoms
           Monom m1 = new IntegerMonom(5,6); // MyClass is tested
           Monom m2=new RealMonom(2,6);
           m1.multiply(m2);
          
            // assert statements
            assertEquals("5x^6*2x^6 must be 10x^12","10x^12",m1.toStr()+"x^"+m1.get_grad());
    }
	@Test
    public void polynom_add_test() {//check addition of two polynomials
           Polynom p1=new Polynom();
           Polynom p2=new Polynom();
          p1.add_coeff(4,7);
          p1.add_coeff(10,11);
          p2.add_coeff(3,3);
          p1.add(p2);
            // assert statements
            assertEquals("p1+p2 must be 4*x^7+10*x^11+3*x^3 ","4*x^7+10*x^11+3*x^3",p1.print_polynom());
    }
	@Test
    public void polynom_subtract_test() {//check subtraction of two polynomials
           Polynom p1=new Polynom();
           Polynom p2=new Polynom();
          p1.add_coeff(4,7);
          p1.add_coeff(10,11);
          p2.add_coeff(3,3);
          p1.subtract(p2);
            // assert statements
            assertEquals("p1-p2 must be 4*x^7+10*x^11-3*x^3 ","4*x^7+10*x^11+-3*x^3",p1.print_polynom());
    }
	@Test
    public void _polynom_multiply() {//check multiplication of two polynomials
           Polynom p1=new Polynom();
           Polynom p2=new Polynom();
          p1.add_coeff(4,7);
          p1.add_coeff(10,11);
          p2.add_coeff(3,3);
          p1.multiply(p2);
            // assert statements

            assertEquals("p1*p2 must be 12*x^10+30*x^14 ","12*x^10+30*x^14",p1.print_polynom());
    }
	@Test
    public void polynom_division() {//check division of two polynomials
           Polynom p1=new Polynom();
           Polynom p2=new Polynom();
          p1.add_coeff(6,7);
          p1.add_coeff(12,11);
          p1.add_coeff(2, 2);
          p2.add_coeff(3,3);
         Polynom quotient= p1.divide(p2);
            // assert statements
            assertEquals("[p1/p2] must be 2*x^2 ","2*x^2",p1.print_polynom());
            assertEquals("p1%p2 must be 4.0*x^8+2.0*x^4  ","4.0*x^8+2.0*x^4",quotient.print_polynom());
    
    }
	@Test
    public void polynom_integration() {//check the integration of a polynomial
           Polynom p1=new Polynom();
          
          p1.add_coeff(6,7);
          p1.add_coeff(12,11);
          p1.add_coeff(2, 2);
          p1.integrate();
            // assert statements
            assertEquals("S(p1)dx must be 0.75*x^8+1*x^12+0.6666666666666666*x^3 ","0.75*x^8+1*x^12+0.6666666666666666*x^3",p1.print_polynom());
    
    }
	@Test
    public void polynom_derivation() {//check the derivation of a polynomial
           Polynom p1=new Polynom();
          
          p1.add_coeff(6,7);
          p1.add_coeff(12,11);
          p1.add_coeff(2, 2);
          p1.derivate();
            // assert statements
          System.out.println(p1.print_polynom());
            assertEquals("dt*p1/dx must be 42*x^6+132*x^10+4*x^1","42*x^6+132*x^10+4*x^1",p1.print_polynom());
    
    }
	
	
	
	
	
	
	

}
