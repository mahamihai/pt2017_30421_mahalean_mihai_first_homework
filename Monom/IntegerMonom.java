
public class IntegerMonom extends Monom{
	
		public IntegerMonom(int nr,int grad)
		{
			super(new Integer(nr),grad);
		
		
		
		}
		@Override
		public String toStr()	//returnes the int value as a string
		{
			return  Integer.toString(this.get_coefficient().intValue());
		}
		
		
		public void sub(IntegerMonom b)//subtracts the integer coefficients
		{
			this.set_coefficient(new Integer(this.get_coefficient().intValue()-b.get_coefficient().intValue()));
			
		}
		public void multiply(IntegerMonom aux)//multiply integer coefficients
		{
			this.set_coefficient(new Integer(this.get_coefficient().intValue()*aux.get_coefficient().intValue()));
			this.set_grad(this.get_grad()+aux.get_grad());
		}
		public  void add(Monom b)//adds the value of another Integer Monom
		{
			
			this.set_coefficient(new Integer(this.get_coefficient().intValue()+b.get_coefficient().intValue()));
		}

}
