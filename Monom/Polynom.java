import javax.swing.JFrame;
import javax.swing.JLabel;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Polynom {
	private ArrayList<Monom> coefficients;

	public Polynom() {

		coefficients = new ArrayList<Monom>();

	}

	public void add_coeff(int coeff, int grad) {
		Monom aux = new IntegerMonom(coeff, grad);
		this.coefficients.add(aux);

	}

	public void add_coeff(Monom aux) {//add a Monom to the coefficients list
		this.coefficients.add(aux);
	}

	public String print_polynom() {//return the form of the Polynom as a string
		String pol_as_string="";
		if(this.coefficients.size()<1)
			{
			return "0";
			}
		for (Monom a : this.coefficients) {
			pol_as_string=pol_as_string+a.toStr() + "*x^" + Integer.toString(a.get_grad())+"+";
		}
	
		return pol_as_string.substring(0, pol_as_string.length() -1);//delete last +
	}

	public void print_polynom_2_gui(JLabel label) {//computes the form of the polynomial and displays it in a label
		label.setText("Answer:");
		if (this.coefficients.size() > 0) {
			for (Monom a : this.coefficients) {
				label.setText(label.getText() + a.toStr() + "*x^" + Integer.toString(a.get_grad()) + "+");//write the Monoms
			}
			label.setText(label.getText().substring(0, label.getText().length() - 1));
		} else {
			label.setText("Answer:0");
		}

	}

	public void add(Polynom b) {//add the coefficients of two polynomials
		for (Monom in : b.coefficients) {
			int found = 0;

			for (Monom a : this.coefficients) {
				if (a.get_grad() == in.get_grad()) {//if terms have the same grade add
					found = 1;
					a.add(in);
					break;

				}
			}
			if (found == 0) {//if not, simply add the term from b to this.coefficients
				this.coefficients.add(in);
			}

		}

	}

	public void subtract(Polynom b) {//subtract from this, Polynom b 

		for (Monom in : b.coefficients) {	//loop through b
			int found = 0;

			for (int i = 0; i < this.coefficients.size(); i++) {	//use for to loop through this.coefficients

				if (this.coefficients.get(i).get_grad() == in.get_grad()) {
					Monom aux = this.coefficients.get(i).real_subtract(in);//make a substraction
					if (aux.get_coefficient().doubleValue() == 0) // check if it's != 0
					{

						this.coefficients.remove(i);
						found = 1;
						i--;	//if deleted reset counter 
					}
					// remove if 0
					else { // add to this if non-zero
						found = 1;
						this.coefficients.set(i, aux);
						break;
					}

				}
			}
			if (found == 0) {//if term has no correspondence add it to this with -
				this.add_coeff(-in.get_coefficient().intValue(), in.get_grad());
				;
			}

		}

	}

	public void multiply(Polynom b) {
		ArrayList<Monom> multiplied = new ArrayList<Monom>();//temporary array 
		for (Monom p1 : this.coefficients) {	//loop through arrays
			for (Monom p2 : b.coefficients) {
				IntegerMonom aux = new IntegerMonom(p1.get_coefficient().intValue() * p2.get_coefficient().intValue(), p1.get_grad() + p2.get_grad());//multiply two Monoms
				int found = 0;
				for (Monom t : multiplied) {//check if term has same grade as a previous multiplication
					if (t.get_grad() == aux.get_grad()) {
						found = 1;	//if found add them
						t.add( aux);
						break;
					}

				}
				if (found == 0) {
					multiplied.add(aux);//if term's grade is unique add it to result polynomial
				}

			}

		}
		this.coefficients = multiplied;

	}

	private Monom create_nr(int n, int grad) {//creates an IntegerMonom
		return new IntegerMonom(n, grad);
	}

	private void multiply_scalar(double scalar) {//multiply all the coefficients with a scalar

		for (Monom a : this.coefficients) {

			a.set_coefficient(new Double(a.get_coefficient().doubleValue() * scalar));
		}
	}

	private Monom create_nr(double n, int grad) {//create a RealMonom from the inputs
		return new RealMonom(n, grad);
	}

	private static Comparator<Monom> getCompByName() {//implement comparator for descending order
		Comparator comp = new Comparator<Monom>() {
			@Override
			public int compare(Monom s1, Monom s2) {
				return (Integer.compare(s2.get_grad(), s1.get_grad()));
			}
		};
		return comp;
	}

	public void integrate() {//integrate this polynom
		Polynom aux = new Polynom();

		for (Monom a : this.coefficients) {//loop through coefficients
			Monom to_add;
			int new_grad = a.get_grad() + 1;
			double new_coefficient = (double) a.get_coefficient().intValue() / new_grad;//update the coefficient Integrate(a*x^b)=a/(b+1)*x^(b+1)
			if ((int) new_coefficient == new_coefficient)
				to_add = create_nr((int) new_coefficient, new_grad);//if int create IntegerMonom
			else
				to_add = create_nr(new_coefficient, new_grad);//if double create RealMonom
			aux.add_coeff(to_add);
			// System.out.println(to_add.toStr());

		}
		this.coefficients = aux.coefficients;

	}

	public void derivate() {//derivate all the terms
		ArrayList<Monom> derivated = new ArrayList<Monom>();
		for (Monom a : this.coefficients) {//loop through coefficients
			if (a.get_grad() != 0)//if grade>0 else don't add to the result
				derivated.add(new IntegerMonom(a.get_coefficient().intValue() * a.get_grad(), a.get_grad() - 1));//derivate(a*x^b)=a*b*x^(b-1)

		}
		this.coefficients = derivated;//copy result to this Polynom
	}

	private void change_all_grades(int change) {//increase or decrease all the grade
		for (Monom a : this.coefficients) {
			a.set_grad(a.get_grad() + change);
		}
	}

	private void sort_pol()

	{
		Collections.sort(this.coefficients, Polynom.getCompByName()); // sort
																		// the
																		// arrays
																		// in
																		// descending
																		// order

	}

	private void copy_coefficients(Polynom b) {

		for (Monom aux : b.coefficients) {
			this.add_coeff(aux.get_coefficient().intValue(), aux.get_grad());

		}
	}

	public Polynom divide(Polynom b) {

		this.sort_pol();			//highest grade on first position
		b.sort_pol();
		Polynom quotient=new Polynom();
		
		while ((this.coefficients.size() > 0)//while first pol !=0 and its grade > grade of denominator
				&& (this.coefficients.get(0).get_grad() >= b.coefficients.get(0).get_grad())) // loop
																								// while
																								// the
																								// grade
																								// is
																								// bigger
		{

			double coeff1 = this.coefficients.get(0).get_coefficient().doubleValue();//max grade nominator
			double coeff2 = b.coefficients.get(0).get_coefficient().doubleValue();//max grade denominator
			Polynom aux = new Polynom();
			
			aux.copy_coefficients(b);			//copy denominator to aux
			aux.multiply_scalar((double)coeff1 / coeff2);		//multiply aux with the division of max coefficients
			
			int grade_difference = this.coefficients.get(0).get_grad() - b.coefficients.get(0).get_grad();//max_grade_a - max_grade_b
			RealMonom tmp=new RealMonom((double)coeff1/coeff2,grade_difference);//save 
			quotient.add_coeff(tmp);	//save term to quotient
			aux.change_all_grades(grade_difference);//change grades of aux to match a's gardes

		
			this.subtract(aux);//a=a-aux
			

		}
		return quotient;
	}
	

}
