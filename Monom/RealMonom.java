
public class RealMonom extends Monom{
	
		public RealMonom(double nr,int grad)
		{
			super(new Double(nr),grad);
		
		
		
		}
		@Override
		public String toStr()
		{
			return Double.toString(this.get_coefficient().doubleValue());//return the double value as string
		}
		public  void add(Monom b)			//add with double coefficientsre mede
		{
			
			this.set_coefficient(new Double(this.get_coefficient().doubleValue()+b.get_coefficient().doubleValue()));

		}

}
