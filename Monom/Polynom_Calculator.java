import java.awt.Button;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class Polynom_Calculator {
	protected Polynom a;
	protected Polynom b;
	protected JTextField polynom_1_text;
	protected JTextField polynom_2_text;
	protected JLabel result_label;

	private void load_polynom_a() {      //read polynom a from textbox
		a = new Polynom();

		String[] aux1 = polynom_1_text.getText().split("\\s");
		for (int i = 0; i < aux1.length; i += 2) {

			a.add_coeff(Integer.parseInt(aux1[i]), Integer.parseInt(aux1[i + 1]));

		}
		//System.out.println("This is pol a\n");
		//a.print_polynom();
		result_label.setText(a.print_polynom());
	}

	private void load_polynom_b() {		//read polynom b from textbox
		b = new Polynom();

		String[] aux1 = polynom_2_text.getText().split("\\s");
		for (int i = 0; i < aux1.length; i += 2) {

			b.add_coeff(Integer.parseInt(aux1[i]), Integer.parseInt(aux1[i + 1]));

		}
		
		//System.out.println("This is pol b\n");
		//b.print_polynom();

	}

	private void setup_gui() {			//initialise the GUI objects

		JFrame jf = new JFrame("Polynomials");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setSize(400, 400);
		jf.setLocation(300, 300);	//setup the frame dimensions
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(Box.createRigidArea(new Dimension(0, 6)));

		polynom_1_text = new JTextField(); // add textbox for first polynom
		panel.add(polynom_1_text);

		polynom_2_text = new JTextField();// text box for second pol
		panel.add(polynom_2_text);

		result_label = new JLabel("Result will appear here"); 	//add result label
		panel.add(result_label);

		Button add_bt = new Button("Add"); // implement add button
		add_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				load_polynom_a();
				load_polynom_b();
				a.add(b);

				a.print_polynom_2_gui(result_label);

			}
		});
		panel.add(add_bt);

		Button sub_bt = new Button("Subtract"); // implement subtract button
		sub_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				load_polynom_a();
				load_polynom_b();
				a.subtract(b);

				a.print_polynom_2_gui(result_label);
			}
		});
		panel.add(sub_bt);

		Button multiply_btn = new Button("Multiply"); // implement multiply
														// button
		multiply_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				load_polynom_a();
				load_polynom_b();
				a.multiply(b);

				a.print_polynom_2_gui(result_label);

			}
		});
		panel.add(multiply_btn);

		Button divide_btn = new Button("Divide"); // implement multiply button
		divide_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				load_polynom_a();
				load_polynom_b();
				Polynom remainder=a.divide(b);
				
				a.print_polynom_2_gui(result_label);
				result_label.setText("Remainder:"+result_label.getText()+"  Quotient:"+remainder.print_polynom());
			}
		});
		panel.add(divide_btn);

		Button derivate_btn = new Button("Derivate");
		derivate_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				load_polynom_a();
				a.derivate();

				a.print_polynom_2_gui(result_label);
				
				

			}
		});

		panel.add(derivate_btn);
		Button integrate_btn = new Button("Integrate");
		integrate_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				load_polynom_a();
				a.integrate();

				a.print_polynom_2_gui(result_label);

			}
		});
		panel.add(integrate_btn);

		jf.add(panel);
		jf.setVisible(true);
	}

	public static void main(String[] args) throws FileNotFoundException {
		Polynom_Calculator main_object = new Polynom_Calculator();

		System.out.println("Starting \n\n");

		main_object.setup_gui();
		

	}

}
