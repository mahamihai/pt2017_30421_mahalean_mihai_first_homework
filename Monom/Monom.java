
public abstract class Monom {
	protected Number coefficient;
	protected int grad;
	protected Monom(Number  nr,int grad)
	{
		this.coefficient=nr;
		this.grad=grad;
	}
	public int get_grad()
	{
		return grad;
	}
	public void set_grad(int grad)
	{
		
		this.grad=grad;
	}
	public  Number get_coefficient()
	{
		return this.coefficient;
	}
	public  void set_coefficient(Number t)
	{
		this.coefficient=t;
	}
	public abstract String  toStr();
	public abstract void add(Monom b)	;		//real add
	
public Monom   real_subtract(Monom b)
{
	double aux=this.get_coefficient().doubleValue()-b.get_coefficient().doubleValue();
	
	if((int)aux==aux)
		
		return new IntegerMonom((int)aux,this.get_grad());
	else
	{
		return new RealMonom(aux,this.get_grad());
		
	}
	
	
	}
public  void multiply (Monom aux)
{
	this.set_coefficient(new Double(this.get_coefficient().doubleValue()*aux.get_coefficient().doubleValue()));
	this.set_grad(this.get_grad()+aux.get_grad());
	
}

	
	

}
